<?php
function tentukan_nilai($number)
{
    $output = " ";
    if ($number >= 98 && $number < 100){
        $output .= "Nilai Sangat Baik";
    } else if ($number >= 75   && $number <  98 ) {
        $output .= "Nilai Baik";
    } else if ($number  >=67  && $number <76 ) {
        $output .= "Nilai Cukup";  
    } else {
        $output .= "Nilai Kurang";
    }
    return $output;
}

//TEST CASES
echo tentukan_nilai(98  ) ; //Sangat Baik
echo "<br>";
echo tentukan_nilai(76 , "<br>");
echo "<br>"; //Baik
echo tentukan_nilai(67 , "<br>");
echo "<br>"; //Cukup
echo tentukan_nilai(43 , "<br>"); //Kurang
?>