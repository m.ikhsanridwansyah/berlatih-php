<?php

require_once ("animal.php");
require_once ("ape.php");
require_once ("frog.php");




$sheep = new Animal("shaun", 2 , "false");
echo  " Nama Hewan = " , $sheep->name; // "shaun"
echo "<br>";
echo $sheep->legs; // 2
echo "<br>";
var_dump($sheep->cold_blooded);// false
echo "<br> <br>";
// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())
//kingkong
$sungokong = new Ape("kera sakti");
echo  " Nama Hewan = " , $sungokong->name;
echo "<br>";
echo $sungokong->legs;
echo "<br>";
$sungokong->yell(); // "Auooo"
echo "<br> <br>";
// kodok
$kodok = new Frog("buduk");
echo  "Nama Hewan = " , $kodok->name;
echo "<br>";
echo $kodok->legs;
echo "<br>";
$kodok->jump();

?>